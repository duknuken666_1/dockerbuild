#!/bin/sh
/opt/unit/sbin/unitd --control unix:/var/run/control.unit.sock --modules /opt/unit/build --state /opt/unit/state

cat /config.json | envsubst | curl -X PUT -d @- --unix-socket /run/control.unit.sock http://localhost/config

tail -f /var/log/unitd.log